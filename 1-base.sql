# PERIODO
insert into `periodo`(`fecha_inicio`,`fecha_termino`) values('2020-01-01','2020-12-31');

# TIPOS DE DOCUMENTO
insert into `rol`(`codigo`,`descripcion`) values('RUT','Identidad Chilena');

# ROLES
insert into `rol`(`codigo`,`descripcion`) values('ADMINISTRADOR','Administrador');
insert into `rol`(`codigo`,`descripcion`) values('PROGRAMADOR','Programador');

# TIPOS DE ACTIVIDADES
insert into `tipo_actividad`(`codigo`,`descripcion`) values('M','Actividad Medica');
insert into `tipo_actividad`(`codigo`,`descripcion`) values('N','Actividad No Medica');
insert into `tipo_actividad`(`codigo`,`descripcion`) values('A','Actividad Medica/No Medica');

# LEYES
insert into `ley`(`codigo`,`descripcion`,`distribucion_horas`) values('18.834','',b'1');
insert into `ley`(`codigo`,`descripcion`,`distribucion_horas`) values('19.664','',b'1');
insert into `ley`(`codigo`,`descripcion`,`distribucion_horas`) values('15.076','',b'0');

# CENTROS DE COSTO
insert into `centro_costo`(`codigo`,`descripcion`) values('0','Centro costo test');

#USUARIOS
insert into `usuario`(`nombre`,`usuario`,`clave`) values('User Admin','admin@ssbb.cl','e10adc3949ba59abbe56e057f20f883e');
insert into `usuario`(`nombre`,`usuario`,`clave`) values('User Observador','observador@ssbb.cl','e10adc3949ba59abbe56e057f20f883e');
insert into `usuario`(`nombre`,`usuario`,`clave`) values('User Aprobador','aprobador@ssbb.cl','e10adc3949ba59abbe56e057f20f883e');
insert into `usuario`(`nombre`,`usuario`,`clave`) values('User Programador','programador@ssbb.cl','e10adc3949ba59abbe56e057f20f883e');